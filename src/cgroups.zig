const file = @import("file.zig");
const fmt = std.fmt.allocPrint;
const fs = std.fs;
const mem = std.mem;
const out = @import("out.zig");
const std = @import("std");

const Allocator = mem.Allocator;
const AutoHashMap = std.AutoHashMap;
const ComptimeStringMap = std.ComptimeStringMap;
const Options = @import("options.zig").Options;
const Str = []const u8;
const Unit = out.Unit;

const Ex = struct {
    desc: Str,
    unit: Unit = .none,
};

const Type = union(enum) {
    simple: Ex,
    stat,
    numa_stat,
    events,
    pressure,
};

const Subsystem = enum { memory, cpuset };

const Req = struct {
    subsystem: Subsystem,
    key: Str,
    of: Type,
};

const Res = struct {
    controller: Str,
    path: Str,
    value: ?Str,
};

const cgroup_fs = "/sys/fs/cgroup";
const group_v1 = "cgroup.v1";
const group_v2 = "cgroup.v2";

const cgroups_v1 = [_]Req{
    .{ .subsystem = .cpuset, .key = "memory_migrate", .of = Type{ .simple = Ex{
        .desc = "if set, move pages to cpusets nodes",
    } } },
    .{ .subsystem = .cpuset, .key = "cpu_exclusive", .of = Type{ .simple = Ex{
        .desc = "is cpu placement exclusive?",
    } } },
    .{ .subsystem = .cpuset, .key = "mem_exclusive", .of = Type{ .simple = Ex{
        .desc = "is memory placement exclusive?",
    } } },
    .{ .subsystem = .cpuset, .key = "mem_hardwall", .of = Type{ .simple = Ex{
        .desc = "is memory allocation hardwalled?",
    } } },
    .{ .subsystem = .cpuset, .key = "memory_pressure", .of = Type{ .simple = Ex{
        .desc = "measure of how much paging pressure in cpuset",
    } } },
    .{ .subsystem = .cpuset, .key = "memory_spread_page", .of = Type{ .simple = Ex{
        .desc = "if set, spread page cache evenly on allowed nodes",
    } } },
    .{ .subsystem = .cpuset, .key = "memory_spread_slab", .of = Type{ .simple = Ex{
        .desc = "if set, spread slab cache evenly on allowed nodes",
    } } },
    .{ .subsystem = .cpuset, .key = "sched_load_balance", .of = Type{ .simple = Ex{
        .desc = "if set, load balance within CPUs on that cpuset",
    } } },
    .{ .subsystem = .cpuset, .key = "sched_relax_domain_level", .of = Type{ .simple = Ex{
        .desc = "the searching range when migrating tasks",
    } } },
    .{ .subsystem = .cpuset, .key = "memory_pressure_enabled", .of = Type{ .simple = Ex{
        .desc = "[ROOT CPUSET ONLY] compute memory_pressure?",
    } } },
    .{ .subsystem = .memory, .key = "usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "current usage for memory",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "memsw.usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "current usage for memory + swap",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "limit_in_bytes", .of = Type{ .simple = Ex{
        .desc = "limit of memory usage",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "memsw.limit_in_bytes", .of = Type{ .simple = Ex{
        .desc = "limit of memory + swap usage",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "failcnt", .of = Type{ .simple = Ex{ .desc = "the number of memory usage hits limits" } } },
    .{ .subsystem = .memory, .key = "memsw.failcnt", .of = Type{ .simple = Ex{
        .desc = "the number of memory + swap hits limits",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "max_usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "max memory usage recorded",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "memsw.max_usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "max memory + swap usage recorded",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "soft_limit_in_bytes", .of = Type{ .simple = Ex{
        .desc = "soft limit of memory usage",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "use_hierarchy", .of = Type{ .simple = Ex{ .desc = "[DEPRECATED] hierarchical account enabled?" } } },
    .{ .subsystem = .memory, .key = "swappiness", .of = Type{ .simple = Ex{
        .desc = "swappiness parameter of vmscan",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "move_charge_at_immigrate", .of = Type{ .simple = Ex{
        .desc = "controls of moving charges",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "oom_control", .of = Type{ .simple = Ex{
        .desc = "oom controls",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.limit_in_bytes", .of = Type{ .simple = Ex{
        .desc = "[DEPRECATED] hard limit for kernel memory",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "current kernel memory allocation",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.failcnt", .of = Type{ .simple = Ex{ .desc = "the number of kernel memory usage hits limits" } } },
    .{ .subsystem = .memory, .key = "kmem.max_usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "max kernel memory usage recorded",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.tcp.limit_in_bytes", .of = Type{ .simple = Ex{
        .desc = "hard limit for TCP buffer memory",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.tcp.usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "current TCP buffer memory allocation",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "kmem.tcp.failcnt", .of = Type{ .simple = Ex{ .desc = "the number of TCP buffer memory usage hits limits" } } },
    .{ .subsystem = .memory, .key = "kmem.tcp.max_usage_in_bytes", .of = Type{ .simple = Ex{
        .desc = "max TCP buffer memory usage recorded",
        .unit = .byte,
    } } },
};

const cgroups_v2 = [_]Req{
    .{ .subsystem = .cpuset, .key = "cpus.effective", .of = Type{ .simple = Ex{
        .desc = "the onlined CPUs that are actually granted",
    } } },
    .{ .subsystem = .cpuset, .key = "mems.effective", .of = Type{ .simple = Ex{
        .desc = "the onlined memory nodes that are actually granted",
    } } },
    .{ .subsystem = .cpuset, .key = "cpus.partition", .of = Type{ .simple = Ex{
        .desc = "",
    } } },
    .{ .subsystem = .memory, .key = "current", .of = Type{ .simple = Ex{
        .desc = "the total amount of memory currently being used",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "min", .of = Type{ .simple = Ex{
        .desc = "hard memory protection",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "low", .of = Type{ .simple = Ex{
        .desc = "best-effort memory protection",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "high", .of = Type{ .simple = Ex{
        .desc = "memory usage throttle limit",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "max", .of = Type{ .simple = Ex{
        .desc = "memory usage hard limit",
        .unit = .byte,
    } } },
    .{ .subsystem = .memory, .key = "oom.group", .of = Type{ .simple = Ex{ .desc = "" } } },
    .{ .subsystem = .memory, .key = "events", .of = .events },
    .{ .subsystem = .memory, .key = "events.local", .of = .events },
    .{ .subsystem = .memory, .key = "swap.current", .of = Type{ .simple = Ex{ .desc = "" } } },
    .{ .subsystem = .memory, .key = "swap.high", .of = Type{ .simple = Ex{ .desc = "" } } },
    .{ .subsystem = .memory, .key = "swap.max", .of = Type{ .simple = Ex{ .desc = "" } } },
    .{ .subsystem = .memory, .key = "swap.events", .of = .events },
    .{ .subsystem = .memory, .key = "pressure", .of = .pressure },
};

const shared_cgroups = [_]Req{
    .{ .subsystem = .cpuset, .key = "cpus", .of = Type{ .simple = Ex{
        .desc = "the requested CPUs",
    } } },
    .{ .subsystem = .cpuset, .key = "mems", .of = Type{ .simple = Ex{
        .desc = "the requested memory nodes",
    } } },
    .{ .subsystem = .memory, .key = "stat", .of = .stat },
    .{ .subsystem = .memory, .key = "numa_stat", .of = .numa_stat },
};

const stat_ex = ComptimeStringMap(Ex, .{
    .{
        "anon",
        .{
            .desc = "amount of memory used in anonymous mappings",
            .unit = .byte,
        },
    },
    .{
        "anon",
        .{
            .desc = "amount of memory used to cache filesystem data",
            .unit = .byte,
        },
    },
    .{
        "kernel_stack",
        .{
            .desc = "amount of memory allocated to kernel stacks",
            .unit = .byte,
        },
    },
    .{
        "pagetables",
        .{
            .desc = "amount of memory allocated for page tables",
            .unit = .byte,
        },
    },
    .{
        "percpu",
        .{
            .desc = "amount of memory used for storing per-cpu kernel data structures",
            .unit = .byte,
        },
    },
    .{
        "sock",
        .{
            .desc = "amount of memory used in network transmission buffers",
            .unit = .byte,
        },
    },
    .{
        "shmem",
        .{
            .desc = "amount of cached filesystem data that is swap-backed",
            .unit = .byte,
        },
    },
    .{
        "file_mapped",
        .{
            .desc = "amount of cached filesystem data mapped with mmap()",
            .unit = .byte,
        },
    },
    .{
        "file_dirty",
        .{
            .desc = "amount of cached modified filesystem data that not yet written back to disk",
            .unit = .byte,
        },
    },
    .{
        "file_writeback",
        .{
            .desc = "amount of cached modified filesystem data that is being written back to disk now",
            .unit = .byte,
        },
    },
    .{
        "swapcached",
        .{ .desc = "amount of swap cached in memory", .unit = .byte },
    },
    .{
        "anon_thp",
        .{
            .desc = "amount of memory used in anonymous mappings backed by transparent hugepages",
            .unit = .byte,
        },
    },
    .{
        "file_thp",
        .{
            .desc = "amount of cached filesystem data backed by transparent hugepages",
            .unit = .byte,
        },
    },
    .{
        "shmem_thp",
        .{
            .desc = "amount of shm, tmpfs, shared anonymous mmap()s backed by transparent hugepages",
            .unit = .byte,
        },
    },
    .{
        "inactive_anon",
        .{
            .desc = "amount of anonymous and swap cache memory on inactive LRU list",
            .unit = .byte,
        },
    },
    .{
        "active_anon",
        .{
            .desc = "amount of anonymous and swap cache memory on active LRU list",
            .unit = .byte,
        },
    },
    .{
        "inactive_file",
        .{
            .desc = "amount of file-backed memory on inactive LRU list",
            .unit = .byte,
        },
    },
    .{
        "active_file",
        .{
            .desc = "amount of file-backed memory on active LRU list",
            .unit = .byte,
        },
    },
    .{
        "unevictable",
        .{
            .desc = "amount of memory that cannot be reclaimed (mlocked etc)",
            .unit = .byte,
        },
    },
    .{
        "slab_reclaimable",
        .{
            .desc = "part of 'slab' that might be reclaimed, such as dentries and inodes",
        },
    },
    .{
        "slab_unreclaimable",
        .{ .desc = "part of 'slab' that cannot be reclaimed on memory pressure" },
    },
    .{
        "slab",
        .{
            .desc = "amount of memory used for storing in-kernel data structures",
            .unit = .byte,
        },
    },
    .{
        "workingset_refault_anon",
        .{ .desc = "number of refaults of previously evicted anonymous pages" },
    },
    .{
        "workingset_refault_file",
        .{ .desc = "number of refaults of previously evicted file pages" },
    },
    .{
        "workingset_activate_anon",
        .{ .desc = "number of refaulted anonymous pages that were immediately activated" },
    },
    .{
        "workingset_activate_file",
        .{ .desc = "number of refaulted file pages that were immediately activated" },
    },
    .{
        "workingset_restore_anon",
        .{ .desc = "number of restored anonymous pages" },
    },
    .{
        "workingset_restore_file",
        .{ .desc = "number of restored file pages" },
    },
    .{
        "workingset_nodereclaim",
        .{
            .desc = "number of times a shadow node has been reclaimed",
        },
    },
    .{
        "pgfault",
        .{
            .desc = "total number of page faults incurred",
        },
    },
    .{
        "pgmajfault",
        .{
            .desc = "number of major page faults incurred",
        },
    },
    .{
        "pgrefill",
        .{
            .desc = "amount of scanned pages in an active LRU list",
        },
    },
    .{
        "pgscan",
        .{
            .desc = "amount of scanned pages in an inactive LRU list",
        },
    },
    .{
        "pgsteal",
        .{
            .desc = "amount of reclaimed pages",
        },
    },
    .{
        "pgactivate",
        .{
            .desc = "amount of pages moved to the active LRU list",
        },
    },
    .{
        "pgdeactivate",
        .{
            .desc = "amount of pages moved to the inactive LRU list",
        },
    },
    .{
        "pglazyfree",
        .{
            .desc = "amount of pages postponed to be freed under memory pressure",
        },
    },
    .{
        "pglazyfreed",
        .{
            .desc = "amount of reclaimed lazyfree pages",
        },
    },
    .{
        "thp_fault_alloc",
        .{ .desc = "number of allocated transparent hugepages to satisfy a page fault" },
    },
    .{
        "thp_collapse_alloc",
        .{ .desc = "number of allocated transparent hugepages to allow collapsing existing pages" },
    },
});

inline fn split(buffer: Str, delimiter: Str) mem.SplitIterator {
    return mem.split(buffer, delimiter);
}

fn normalizeCGroupPath(opt_path: ?Str) ?Str {
    if (opt_path) |path| {
        const trimmed = mem.trimRight(u8, path, "\n");
        return mem.trimLeft(u8, trimmed, "/");
    }
    return null;
}

fn getCGroupPathForV1(alloc: *Allocator, pid: Str, subsystem: Str) !?Str {
    const path = try fmt(alloc, "/proc/{s}/cgroup", .{pid});
    var f = try fs.openFileAbsolute(path, .{});
    defer f.close();

    var buffer: [4096]u8 = undefined;
    while (try f.reader().readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        var parts = split(line, ":");
        _ = parts.next();
        if (parts.next()) |part| {
            if (mem.eql(u8, part, subsystem)) {
                return normalizeCGroupPath(parts.next());
            }
        }
    }
    return null;
}

fn print(alloc: *Allocator, req: Req, res: Res, options: Options) !void {
    const simple_print = (struct {
        alloc: *Allocator,
        options: Options,
        res: Res,
        fn func(self: @This(), key: Str, value: Str, unit: Unit, desc: ?Str) !void {
            const title = try fmt(
                self.alloc,
                "{s}::{s}",
                .{ self.res.controller, key },
            );
            try out.println(
                self.alloc,
                title,
                self.res.path,
                value,
                unit,
                desc,
                self.options,
            );
        }
    }{ .alloc = alloc, .options = options, .res = res }).func;
    if (res.value) |value| {
        const val = mem.trimRight(u8, value, "\n");
        switch (req.of) {
            .simple => |ex| try simple_print(req.key, val, ex.unit, ex.desc),
            .stat => {
                var iter = split(val, "\n");
                while (iter.next()) |item| {
                    var parts = split(item, " ");
                    const stat_key = parts.next().?;
                    const key = try fmt(alloc, "{s}.{s}", .{ req.key, stat_key });
                    const item_val = parts.next().?;
                    const ex = stat_ex.get(stat_key) orelse Ex{ .desc = "N/A" };
                    try simple_print(key, item_val, ex.unit, ex.desc);
                }
            },
            .events => {
                var iter = split(val, "\n");
                while (iter.next()) |item| {
                    var parts = split(item, " ");
                    const stat_key = parts.next().?;
                    const key = try fmt(alloc, "{s}.{s}", .{ req.key, stat_key });
                    const item_val = parts.next().?;
                    // TODO: add desc
                    try simple_print(key, item_val, .none, null);
                }
            },
            .numa_stat => {
                var iter = split(val, "\n");
                while (iter.next()) |item| {
                    var parts = split(item, " ");
                    const stat_key = parts.next().?;
                    const key = try fmt(alloc, "{s}.{s}", .{ req.key, stat_key });
                    const ex = stat_ex.get(stat_key) orelse Ex{ .desc = "N/A" };
                    while (parts.next()) |part| {
                        var n_parts = split(part, "=");
                        const n_key = try fmt(
                            alloc,
                            "{s}.{s}",
                            .{ key, n_parts.next().? },
                        );
                        const n_val = n_parts.next().?;
                        try simple_print(n_key, n_val, ex.unit, ex.desc);
                    }
                }
            },
            .pressure => {
                var iter = split(val, "\n");
                while (iter.next()) |item| {
                    var parts = split(item, " ");
                    const stat_key = parts.next().?;
                    const key = try fmt(alloc, "{s}.{s}", .{ req.key, stat_key });
                    while (parts.next()) |part| {
                        var n_parts = split(part, "=");
                        const n_key = try fmt(
                            alloc,
                            "{s}.{s}",
                            .{ key, n_parts.next().? },
                        );
                        const n_val = n_parts.next().?;
                        // TODO: add desc
                        try simple_print(n_key, n_val, .none, null);
                    }
                }
            },
        }
    } else {
        const desc = switch (req.of) {
            .simple => |ex| ex.desc,
            else => null,
        };
        try simple_print(req.key, "N/A", .none, desc);
    }
}

fn infoCGroupForV1(
    alloc: *Allocator,
    subsystem: Str,
    req: Req,
    path: Str,
    options: Options,
) !void {
    const controller = try fmt(alloc, "{s}.{s}", .{ group_v1, subsystem });
    if (mem.startsWith(u8, controller, options.run.get())) {
        const controller_path = try fs.path.join(
            alloc,
            &[_]Str{ cgroup_fs, subsystem },
        );
        const controller_key = try fmt(
            alloc,
            "{s}.{s}",
            .{ subsystem, req.key },
        );
        var cgroup_path = try fs.path.join(
            alloc,
            &[_]Str{ controller_path, path, controller_key },
        );
        var value = file.read(alloc, cgroup_path);
        if (value == null) {
            cgroup_path = try fs.path.join(
                alloc,
                &[_]Str{ controller_path, controller_key },
            );
            value = file.read(alloc, cgroup_path);
        }
        try print(alloc, req, Res{
            .controller = controller,
            .path = cgroup_path,
            .value = value,
        }, options);
    }
}

fn infoCGroupForV2(
    alloc: *Allocator,
    subsystem: Str,
    req: Req,
    path: Str,
    options: Options,
) !void {
    const controller = try fmt(alloc, "{s}.{s}", .{ group_v2, subsystem });
    if (mem.startsWith(u8, controller, options.run.get())) {
        const controller_key = try fmt(
            alloc,
            "{s}.{s}",
            .{ subsystem, req.key },
        );
        var cgroup_path = try fs.path.join(
            alloc,
            &[_]Str{ cgroup_fs, path, controller_key },
        );
        var value = file.read(alloc, cgroup_path);
        if (value == null) {
            cgroup_path = try fs.path.join(
                alloc,
                &[_]Str{ cgroup_fs, controller_key },
            );
            value = file.read(alloc, cgroup_path);
        }
        try print(alloc, req, Res{
            .controller = controller,
            .path = cgroup_path,
            .value = value,
        }, options);
    }
}

fn infoForV1(alloc: *Allocator, options: Options) !void {
    if (file.exists(cgroup_fs)) {
        var map = AutoHashMap(Subsystem, ?Str).init(alloc);
        defer map.deinit();

        for (cgroups_v1 ++ shared_cgroups) |group| {
            const subsystem = group.subsystem;
            const sys = @tagName(subsystem);
            if (!map.contains(subsystem)) {
                const opt_path = try getCGroupPathForV1(alloc, options.pid.get(), sys);
                if (opt_path == null) {
                    const key = try fmt(alloc, "{s}.{s}::*", .{ group_v1, sys });
                    try out.println(alloc, key, null, "N/A", .none, null, options);
                }
                try map.put(subsystem, opt_path);
            }
            if (map.get(subsystem).?) |path| {
                try infoCGroupForV1(alloc, sys, group, path, options);
            }
        }
    } else {
        const key = try fmt(alloc, "{s}::*", .{group_v1});
        try out.println(alloc, key, null, "N/A", .none, null, options);
    }
}

fn infoForV2(alloc: *Allocator, options: Options) !void {
    const prefix = "0::";
    const cgroup_path = try fmt(alloc, "/proc/{s}/cgroup", .{options.pid.get()});
    const cgroup_file = file.read(alloc, cgroup_path);

    if (file.exists(cgroup_fs) and
        cgroup_file != null and
        mem.startsWith(u8, cgroup_file.?, prefix))
    {
        const path = normalizeCGroupPath(cgroup_file.?[prefix.len..]).?;
        for (cgroups_v2 ++ shared_cgroups) |group| {
            const sys = @tagName(group.subsystem);
            try infoCGroupForV2(alloc, sys, group, path, options);
        }
    } else {
        const key = try fmt(alloc, "{s}::*", .{group_v2});
        try out.println(alloc, key, null, "N/A", .none, null, options);
    }
}

pub fn info(alloc: *Allocator, options: Options) !void {
    if (mem.startsWith(u8, group_v1, options.run.get())) {
        try infoForV1(alloc, options);
    }

    if (mem.startsWith(u8, group_v2, options.run.get())) {
        try infoForV2(alloc, options);
    }
}
