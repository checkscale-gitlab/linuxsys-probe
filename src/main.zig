const bytes = @import("bytes.zig");
const c = @import("c.zig");
const cgroups = @import("cgroups.zig");
const debug = std.debug;
const file = @import("file.zig");
const fmt = std.fmt.allocPrint;
const heap = std.heap;
const mem = std.mem;
const os = std.os;
const out = @import("out.zig");
const std = @import("std");
const time = std.time;

const Allocator = mem.Allocator;
const ArenaAllocator = heap.ArenaAllocator;
const ArrayList = std.ArrayList;
const Options = @import("options.zig").Options;
const ShiftInt = std.math.Log2Int(usize);
const Str = []const u8;
const Thread = std.Thread;
const Unit = out.Unit;

const probe_cpu_group = "probe.cpu";
const proc_stat_group = "proc.stat";

fn cpuIsSet(cpu: usize, set: *os.cpu_set_t) bool {
    // TODO: use CPU_ISSET, zig can't parse it yet
    // https://github.com/ziglang/zig/issues/1085
    const mask_index = cpu >> @bitSizeOf(ShiftInt);
    const mask_bit = @as(usize, 1) << @truncate(ShiftInt, cpu);
    return (set[mask_index] & mask_bit) != 0;
}

fn cpuSet(cpu: usize, set: *os.cpu_set_t) void {
    // TODO: use CPU_SET, zig can't parse it yet
    // https://github.com/ziglang/zig/issues/1085
    const mask_index = cpu >> @bitSizeOf(ShiftInt);
    const mask_bit = @as(usize, 1) << @truncate(ShiftInt, cpu);
    set[mask_index] |= mask_bit;
}

fn readCpuList(alloc: *Allocator) !ArrayList(usize) {
    var cpu_set = try os.sched_getaffinity(0);
    var cpu_list = ArrayList(usize).init(alloc);
    var i: usize = 0;
    while (i < c.CPU_SETSIZE) : (i += 1) {
        if (cpuIsSet(i, &cpu_set)) {
            try cpu_list.append(i);
        }
    }
    return cpu_list;
}

fn dumpArray(
    comptime T: type,
    alloc: *Allocator,
    arr: []T,
    opt_current: ?T,
) !Str {
    var str = ArrayList(u8).init(alloc);
    try str.append('[');
    var first = true;
    for (arr) |elem| {
        if (first) {
            first = false;
        } else {
            try str.writer().print(", ", .{});
        }
        if (opt_current) |current| {
            if (current == elem) {
                try str.append('*');
            }
        }
        try str.writer().print("{}", .{elem});
    }
    try str.append(']');
    return str.items[0..];
}

fn cpuinfo(alloc: *Allocator, options: Options) !usize {
    const group = "proc.cpuinfo";
    const proc_cpuinfo = "/proc/cpuinfo";
    var f = try std.fs.openFileAbsolute(proc_cpuinfo, .{});
    defer f.close();

    var processors: usize = 0;
    var buffer: [4096]u8 = undefined;
    while (try f.reader().readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        processors += @boolToInt(mem.startsWith(u8, line, "processor"));
    }

    if (mem.startsWith(u8, group, options.run.get())) {
        const println = (struct {
            alloc: *Allocator,
            options: Options,
            fn println(self: @This(), key: Str, value: Str, desc: ?Str) !void {
                const title = try fmt(self.alloc, "{s}::{s}", .{ group, key });
                try out.println(
                    self.alloc,
                    title,
                    null,
                    value,
                    Unit.none,
                    desc,
                    self.options,
                );
            }
        }{ .alloc = alloc, .options = options }).println;
        const value = try fmt(alloc, "{}", .{processors});
        try println("num_cores", value, "total CPUs");

        const cpu_list = try readCpuList(alloc);
        const printed = try dumpArray(usize, alloc, cpu_list.items, null);
        try println("affinity", printed, null);
    }
    return processors;
}

fn setAffinity(id: usize) void {
    var set: os.cpu_set_t = undefined;
    for (set) |*b| {
        b.* = 0;
    }
    cpuSet(id, &set);
    _ = c.sched_setaffinity(
        0,
        @sizeOf(os.cpu_set_t),
        @ptrCast(*c.cpu_set_t, &set),
    );
}

const CpuProbeReq = struct {
    id: usize,
    no_humanize: bool,
};

fn cpuProbeThread(args: CpuProbeReq) !void {
    var arena = ArenaAllocator.init(heap.c_allocator);
    defer arena.deinit();
    var alloc: *Allocator = &arena.allocator;

    setAffinity(args.id);
    const cpu_list = try readCpuList(alloc);
    const current = @intCast(usize, c.sched_getcpu());
    if (args.no_humanize) {
        debug.print("{s}::affinity [{}] ", .{ probe_cpu_group, args.id });
    } else {
        debug.print("{s}::affinity [{}] = ", .{ probe_cpu_group, args.id });
    }
    const printed = try dumpArray(usize, alloc, cpu_list.items, current);
    debug.print("{s}\n", .{printed});
}

fn cpuProbe(processors: usize, options: Options) !void {
    if (mem.startsWith(u8, probe_cpu_group, options.run.get())) {
        var i: usize = 0;
        while (i < processors) : (i += 1) {
            const req = CpuProbeReq{
                .id = i,
                .no_humanize = options.no_humanize,
            };
            const thread = try Thread.spawn(cpuProbeThread, req);
            thread.wait();
        }
    }
}

fn procStatInfo(alloc: *Allocator, options: Options) !void {
    if (mem.startsWith(u8, proc_stat_group, options.run.get())) {
        const println = (struct {
            alloc: *Allocator,
            options: Options,
            fn func(self: @This(), key: Str, value: Str, unit: Unit, desc: ?Str) !void {
                const title = try fmt(
                    self.alloc,
                    "{s}::{s}",
                    .{ proc_stat_group, key },
                );
                try out.println(
                    self.alloc,
                    title,
                    null,
                    value,
                    unit,
                    desc,
                    self.options,
                );
            }
        }{ .alloc = alloc, .options = options }).func;

        const path = try fmt(alloc, "/proc/{s}/stat", .{options.pid.get()});
        if (file.read(alloc, path)) |text| {
            const ticks_per_second = @intCast(c_ulong, c.sysconf(c._SC_CLK_TCK));
            const page_size = @intCast(c_ulong, c.sysconf(c._SC_PAGESIZE));

            var iter = mem.split(text, " ");
            // var pid: c_int = 0;
            // var state: u8 = 0;
            // var ppid: c_int = 0;
            // var pgrp: c_int = 0;
            // var session: c_int = 0;
            // var tty_nr: c_int = 0;
            // var tpgid: c_int = 0;
            // var flags: c_uint = 0;
            // var minflt: c_ulong = 0;
            // var cminflt: c_ulong = 0;
            // var majflt: c_ulong = 0;
            // var cmajflt: c_ulong = 0;
            // var utime: c_ulong = 0;
            // var stime: c_ulong = 0;
            // var cutime: c_long = 0;
            // var cstime: c_long = 0;
            // var priority: c_long = 0;
            // var nice: c_long = 0;
            // var num_threads: c_long = 0;
            // var itrealvalue: c_long = 0;
            // var starttime: c_ulonglong = 0;
            // var vsize: c_ulong = 0;
            // var rss: c_long = 0;
            // var rsslim: c_ulong = 0;
            // var startcode: c_ulong = 0;
            // var endcode: c_ulong = 0;
            // var startstack: c_ulong = 0;
            // var kstkesp: c_ulong = 0;
            // var kstkeip: c_ulong = 0;
            // var signal: c_ulong = 0;
            // var blocked: c_ulong = 0;
            // var sigignore: c_ulong = 0;
            // var sigcatch: c_ulong = 0;
            // var wchan: c_ulong = 0;
            // var nswap: c_ulong = 0;
            // var cnswap: c_ulong = 0;
            // var exit_signal: c_int = 0;
            // var processor: c_int = 0;
            // var rt_priority: c_uint = 0;
            // var policy: c_uint = 0;
            // var delayacct_blkio_ticks: c_ulonglong = 0;
            // var guest_time: c_ulong = 0;
            // var cguest_time: c_long = 0;
            // var start_data: c_ulong = 0;
            // var end_data: c_ulong = 0;
            // var start_brk: c_ulong = 0;
            // var arg_start: c_ulong = 0;
            // var arg_end: c_ulong = 0;
            // var env_start: c_ulong = 0;
            // var env_end: c_ulong = 0;
            // var exit_code: c_int = 0;

            if (options.verbose) {
                debug.print("{s}: {s}\n", .{ path, text });
            }
            // const scanned = c.sscanf(
            //     text,
            //     "%d %s %c %d %d %d %d %d %u %lu %lu %lu %lu %lu %lu %ld %ld %ld %ld %ld %ld %ld %llu %lu %ld %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %d %d %u %u %llu %lu %ld %lu %lu %lu %lu %lu %lu %d",
            //     &pid,
            //     comm,
            //     &state,
            //     &ppid,
            //     &pgrp,
            //     &session,
            //     &tty_nr,
            //     &tpgid,
            //     &flags,
            //     &minflt,
            //     &cminflt,
            //     &majflt,
            //     &cmajflt,
            //     &utime,
            //     &stime,
            //     &cutime,
            //     &cstime,
            //     &priority,
            //     &nice,
            //     &num_threads,
            //     &itrealvalue,
            //     &starttime,
            //     &vsize,
            //     &rss,
            //     &rsslim,
            //     &startcode,
            //     &endcode,
            //     &startstack,
            //     &kstkesp,
            //     &kstkeip,
            //     &signal,
            //     &blocked,
            //     &sigignore,
            //     &sigcatch,
            //     &wchan,
            //     &nswap,
            //     &cnswap,
            //     &exit_signal,
            //     &processor,
            //     &rt_priority,
            //     &policy,
            //     &delayacct_blkio_ticks,
            //     &guest_time,
            //     &cguest_time,
            //     &start_data,
            //     &end_data,
            //     &start_brk,
            //     &arg_start,
            //     &arg_end,
            //     &env_start,
            //     &env_end,
            //     &exit_code,
            // );
            // if (scanned > 0) {
            try println(
                "pid",
                iter.next() orelse "N/A",
                .none,
                "the process ID",
            );

            var comm = try alloc.dupe(u8, iter.next() orelse "()");
            var brace_count = mem.count(u8, comm, "(");
            while (brace_count > 0 and brace_count != mem.count(u8, comm, ")")) {
                const next = iter.next() orelse "()";
                brace_count = mem.count(u8, comm, "(");
                comm = try fmt(alloc, "{s} {s}", .{ comm, next });
            }
            try println(
                "comm",
                comm,
                .none,
                "the filename of the executable",
            );

            try println(
                "state",
                iter.next() orelse "N/A",
                .none,
                "the process state",
            );
            try println(
                "ppid",
                iter.next() orelse "N/A",
                .none,
                "the PID of the parent of this process",
            );
            try println(
                "pgrp",
                iter.next() orelse "N/A",
                .none,
                "the process group ID of the process",
            );
            try println(
                "session",
                iter.next() orelse "N/A",
                .none,
                "the session ID of the process",
            );
            try println(
                "tty_nr",
                iter.next() orelse "N/A",
                .none,
                "the process terminal",
            );
            try println(
                "tpgid",
                iter.next() orelse "N/A",
                .none,
                "the process group ID of the process terminal",
            );
            try println(
                "flags",
                iter.next() orelse "N/A",
                .none,
                "the kernel flags word of the process",
            );
            try println(
                "minflt",
                iter.next() orelse "N/A",
                .none,
                "the minor faults the process has made",
            );
            try println(
                "cminflt",
                iter.next() orelse "N/A",
                .none,
                "the minor faults the process' children have made",
            );
            try println(
                "majflt",
                iter.next() orelse "N/A",
                .none,
                "the major faults the process has made",
            );
            try println(
                "cmajflt",
                iter.next() orelse "N/A",
                .none,
                "the major faults the process' children have made",
            );

            var utime: ?c_ulong = null;
            if (iter.next()) |i| {
                utime = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "utime",
                if (utime != null)
                    try fmt(alloc, "{}", .{utime.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "amount of time that the process has been scheduled in user mode",
            );

            var stime: ?c_ulong = null;
            if (iter.next()) |i| {
                stime = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "stime",
                if (stime != null)
                    try fmt(alloc, "{}", .{stime.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "amount of time that the process has been scheduled in kernel mode",
            );

            var cutime: ?c_ulong = null;
            if (iter.next()) |i| {
                cutime = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "cutime",
                if (cutime != null)
                    try fmt(alloc, "{}", .{cutime.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "amount of time that the process' children have been scheduled in user mode",
            );

            var cstime: ?c_ulong = null;
            if (iter.next()) |i| {
                cstime = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "cstime",
                if (cstime != null)
                    try fmt(alloc, "{}", .{cstime.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "amount of time that the process' children have been scheduled in kernel mode",
            );

            try println(
                "priority",
                iter.next() orelse "N/A",
                .none,
                "the scheduling priority",
            );
            try println(
                "nice",
                iter.next() orelse "N/A",
                .none,
                "the nice value",
            );
            try println(
                "num_threads",
                iter.next() orelse "N/A",
                .none,
                "number of threads in this process",
            );
            // itrealvalue, not maintained
            _ = iter.next();

            var starttime: ?c_ulong = null;
            if (iter.next()) |i| {
                starttime = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "starttime",
                if (starttime != null)
                    try fmt(alloc, "{}", .{starttime.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "the time the process started after system boot",
            );

            var vsize: ?c_ulong = null;
            if (iter.next()) |i| {
                vsize = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "vsize",
                if (vsize != null)
                    try fmt(alloc, "{}", .{vsize.?})
                else
                    "N/A",
                .byte,
                "virtual memory size",
            );

            var rss: ?c_ulong = null;
            if (iter.next()) |i| {
                rss = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "rss",
                if (rss != null)
                    try fmt(alloc, "{}", .{rss.? * page_size})
                else
                    "N/A",
                .byte,
                "resident set size",
            );

            var rsslim: ?c_ulong = null;
            if (iter.next()) |i| {
                rsslim = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "rsslim",
                if (rss != null)
                    try fmt(alloc, "{}", .{rsslim.?})
                else
                    "N/A",
                .byte,
                "current soft limit in bytes on the RSS of the process",
            );

            try println(
                "startcode",
                iter.next() orelse "N/A",
                .none,
                "the address above which program text can run",
            );
            try println(
                "endcode",
                iter.next() orelse "N/A",
                .none,
                "the address below which program text can run",
            );
            try println(
                "startstack",
                iter.next() orelse "N/A",
                .none,
                "the address of the start of the stack",
            );
            try println(
                "kstkesp",
                iter.next() orelse "N/A",
                .none,
                "the current value of ESP (stack pointer)",
            );
            try println(
                "kstkeip",
                iter.next() orelse "N/A",
                .none,
                "the current EIP (instruction pointer)",
            );
            try println(
                "signal",
                iter.next() orelse "N/A",
                .none,
                "the bitmap of pending signals",
            );
            try println(
                "blocked",
                iter.next() orelse "N/A",
                .none,
                "the bitmap of blocked signals",
            );
            try println(
                "sigignore",
                iter.next() orelse "N/A",
                .none,
                "the bitmap of ignored signals",
            );
            try println(
                "sigcatch",
                iter.next() orelse "N/A",
                .none,
                "he bitmap of caught signals",
            );
            try println(
                "wchan",
                iter.next() orelse "N/A",
                .none,
                "the 'channel' in which the process is waiting",
            );
            // nswap, not maintained
            _ = iter.next();
            // cnswap, not maintained
            _ = iter.next();
            try println(
                "exit_signal",
                iter.next() orelse "N/A",
                .none,
                "signal to be sent to parent when we die",
            );
            try println(
                "processor",
                iter.next() orelse "N/A",
                .none,
                "CPU number last executed on",
            );
            try println(
                "rt_priority",
                iter.next() orelse "N/A",
                .none,
                "real-time scheduling priority",
            );
            try println(
                "policy",
                iter.next() orelse "N/A",
                .none,
                "scheduling policy",
            );

            var delayacct_blkio_ticks: ?c_ulong = null;
            if (iter.next()) |i| {
                delayacct_blkio_ticks = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "delayacct_blkio_ticks",
                if (delayacct_blkio_ticks != null)
                    try fmt(alloc, "{}", .{delayacct_blkio_ticks.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "aggregated block I/O delays",
            );

            var guest_time: ?c_ulong = null;
            if (iter.next()) |i| {
                guest_time = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "guest_time",
                if (guest_time != null)
                    try fmt(alloc, "{}", .{guest_time.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "guest time of the process",
            );

            var cguest_time: ?c_ulong = null;
            if (iter.next()) |i| {
                cguest_time = try std.fmt.parseInt(c_ulong, i, 10);
            }
            try println(
                "cguest_time",
                if (cguest_time != null)
                    try fmt(alloc, "{}", .{cguest_time.? / ticks_per_second})
                else
                    "N/A",
                .second,
                "guest time of the process's children",
            );

            try println(
                "start_data",
                iter.next() orelse "N/A",
                .none,
                "address above which program initialized and uninitialized data are placed",
            );
            try println(
                "end_data",
                iter.next() orelse "N/A",
                .none,
                "address below which program initialized and uninitialized data are placed",
            );
            try println(
                "start_brk",
                iter.next() orelse "N/A",
                .none,
                "address above which program heap can be expanded with brk",
            );
            try println(
                "arg_start",
                iter.next() orelse "N/A",
                .none,
                "address above which program command-line arguments (argv) are placed",
            );
            try println(
                "arg_end",
                iter.next() orelse "N/A",
                .none,
                "address below program command-line arguments (argv) are placed",
            );
            try println(
                "env_start",
                iter.next() orelse "N/A",
                .none,
                "address above which program environment is placed",
            );
            try println(
                "env_end",
                iter.next() orelse "N/A",
                .none,
                "address below which program environment is placed",
            );
            try println(
                "exit_code",
                iter.next() orelse "N/A",
                .none,
                "the thread's exit status in the form reported by waitpid",
            );
        } else {
            try println("*", "N/A", .none, null);
        }
    }
}

fn procSysVmInfo(alloc: *Allocator, options: Options) !u8 {
    const read = (struct {
        alloc: *Allocator,
        options: Options,
        fn func(self: @This(), key: Str) !Str {
            return file.read(
                self.alloc,
                try fmt(self.alloc, "/proc/sys/vm/{s}", .{key}),
            ) orelse "N/A";
        }
    }{ .alloc = alloc, .options = options }).func;
    const panic_on_oom = try std.fmt.parseInt(
        u8,
        try read("panic_on_oom"),
        10,
    );

    const group = "proc.sys.vm";
    if (mem.startsWith(u8, group, options.run.get())) {
        const println = (struct {
            alloc: *Allocator,
            options: Options,
            fn func(self: @This(), key: Str, value: Str, unit: Unit, desc: ?Str) !void {
                const title = try fmt(self.alloc, "{s}::{s}", .{ group, key });
                try out.println(
                    self.alloc,
                    title,
                    null,
                    value,
                    unit,
                    desc,
                    self.options,
                );
            }
        }{ .alloc = alloc, .options = options }).func;

        const admin_reserve_kbytes = 1024 * try std.fmt.parseInt(
            u32,
            try read("admin_reserve_kbytes"),
            10,
        );
        try println(
            "admin_reserve_kbytes",
            try fmt(alloc, "{}", .{admin_reserve_kbytes}),
            .byte,
            "free memory amount that should be reserved for cap_sys_admin users",
        );

        const oom_desc = switch (panic_on_oom) {
            0 => "oom-killer only",
            1 => "oom-killer first, then kernel panic",
            2 => "kernel panic",
            else => "unknown",
        };
        try println(
            "panic_on_oom",
            try fmt(alloc, "{}", .{panic_on_oom}),
            .none,
            oom_desc,
        );

        try println(
            "max_map_count",
            try read("max_map_count"),
            .none,
            "the maximum number of memory map areas a process may have",
        );
    }
    return panic_on_oom;
}

fn sysInfo(alloc: *Allocator, processors: usize, options: Options) !usize {
    const group = "sysinfo";
    var info: c.struct_sysinfo = undefined;
    @memset(@ptrCast([*]u8, &info), 0, @sizeOf(c.struct_sysinfo));
    const err = c.sysinfo(&info);
    const mem_unit = info.mem_unit;
    const freeram = info.freeram * mem_unit;
    if (mem.startsWith(u8, group, options.run.get())) {
        const println = (struct {
            alloc: *Allocator,
            options: Options,
            fn func(self: @This(), key: Str, value: Str, unit: Unit, desc: ?Str) !void {
                const title = try fmt(self.alloc, "{s}::{s}", .{ group, key });
                try out.println(
                    self.alloc,
                    title,
                    null,
                    value,
                    unit,
                    desc,
                    self.options,
                );
            }
        }{ .alloc = alloc, .options = options }).func;
        if (err != 0) {
            try println("*", "N/A", .none, null);
        } else {
            const f_load = @as(f32, 100.0) / (1 << c.SI_LOAD_SHIFT) / @intToFloat(f32, processors);
            try println(
                "uptime",
                try fmt(alloc, "{}", .{info.uptime}),
                .second,
                "seconds since boot",
            );
            try println(
                "loads",
                try fmt(alloc, "{d:.2}% {d:.2}% {d:.2}%", .{
                    f_load * @intToFloat(f32, info.loads[0]),
                    f_load * @intToFloat(f32, info.loads[1]),
                    f_load * @intToFloat(f32, info.loads[2]),
                }),
                .none,
                "1, 5, and 15 minute load averages",
            );
            try println(
                "procs",
                try fmt(alloc, "{}", .{info.procs}),
                .none,
                "number of current processes",
            );
            try println(
                "totalram",
                try fmt(alloc, "{}", .{info.totalram * mem_unit}),
                .byte,
                "total usable main memory size",
            );
            try println(
                "freeram",
                try fmt(alloc, "{}", .{freeram}),
                .byte,
                "available memory size",
            );
            try println(
                "sharedram",
                try fmt(alloc, "{}", .{info.sharedram * mem_unit}),
                .byte,
                "amount of shared memory",
            );
            try println(
                "bufferram",
                try fmt(alloc, "{}", .{info.bufferram * mem_unit}),
                .byte,
                "memory used by buffers",
            );
            try println(
                "totalswap",
                try fmt(alloc, "{}", .{info.totalswap * mem_unit}),
                .byte,
                "total swap space size",
            );
            try println(
                "freeswap",
                try fmt(alloc, "{}", .{info.freeswap * mem_unit}),
                .byte,
                "swap space still available",
            );
            try println(
                "totalhigh",
                try fmt(alloc, "{}", .{info.totalhigh * mem_unit}),
                .byte,
                "total high memory size",
            );
            try println(
                "freehigh",
                try fmt(alloc, "{}", .{info.freehigh * mem_unit}),
                .byte,
                "available high memory size",
            );
        }
    }
    return freeram;
}

fn memFill(size: usize) void {
    const num_allocs = 1000;
    const chunk_size = size / num_allocs;
    // c_allocator shows segfault debug messages
    const alloc = heap.raw_c_allocator;

    var ptrs = ArrayList(?[]u8).init(alloc);
    ptrs.appendNTimes(null, num_allocs) catch std.c.exit(c.EXIT_FAILURE);

    var success = true;
    for (ptrs.items) |*opt_ptr| {
        opt_ptr.* = alloc.alloc(u8, chunk_size) catch null;
        if (opt_ptr.*) |ptr| {
            @memset(ptr.ptr, 10, chunk_size);
        } else {
            success = false;
            break;
        }
    }

    for (ptrs.items) |opt_ptr| {
        if (opt_ptr) |ptr| {
            alloc.free(ptr);
        } else {
            break;
        }
    }
    ptrs.deinit();

    std.c.exit(if (success) c.EXIT_SUCCESS else c.EXIT_FAILURE);
}

fn wait() bool {
    var status: c_int = c.EXIT_FAILURE;
    return c.wait(&status) > 0 and
        os.WIFEXITED(@intCast(u32, status)) and
        os.WEXITSTATUS(@intCast(u32, status)) == c.EXIT_SUCCESS;
}

fn memProbe(alloc: *Allocator, freeram: usize, options: Options) !void {
    const group = "probe.mem";
    if (mem.startsWith(u8, group, options.run.get())) {
        if (options.verbose) {
            debug.print(
                "INFO: {s} uses --delta=={s}\n",
                .{ group, try bytes.toString(alloc, options.mem_delta) },
            );
        }
        var high: usize = freeram;
        var low: usize = 1;
        while (high - low > options.mem_delta) {
            const size = (low + high) / 2;
            const pid = std.c.fork();
            if (pid == 0) {
                if (options.no_humanize) {
                    debug.print("{s}::alloc {} ", .{ group, size });
                } else {
                    debug.print(
                        "{s}::alloc {s} = ",
                        .{ group, try bytes.toString(alloc, size) },
                    );
                }
                memFill(size);
            } else if (wait()) {
                debug.print("{}\n", .{true});
                low = size + 1;
            } else {
                debug.print("{}\n", .{false});
                high = size - 1;
            }
        }
    }
}

fn exitWaitThread(running: *bool) !void {
    const stdin = std.io.getStdIn().reader();
    var buf: [10]u8 = undefined;
    _ = try stdin.readUntilDelimiterOrEof(buf[0..], '\n');
    @atomicStore(bool, running, false, .SeqCst);
}

pub fn main() !void {
    var running: bool = true;
    const thread = try Thread.spawn(exitWaitThread, &running);

    var arena = ArenaAllocator.init(heap.c_allocator);
    defer arena.deinit();
    var alloc: *Allocator = &arena.allocator;

    const options = try Options.init(alloc);
    defer options.deinit();

    while (@atomicLoad(bool, &running, .SeqCst)) {
        try cgroups.info(alloc, options);
        const processors = try cpuinfo(alloc, options);
        try procStatInfo(alloc, options);

        const oom_mode = try procSysVmInfo(alloc, options);
        const freeram = try sysInfo(alloc, processors, options);

        if (options.interval) |i| {
            time.sleep(i * time.ns_per_s);
        } else {
            try cpuProbe(processors, options);
            if (oom_mode > 0 and !options.force and options.verbose) {
                const warning =
                    \\Memory probing disabled as it could cause a kernel panic.
                    \\Please use --force flag to do the probing anyway.
                    \\
                ;
                debug.print("{s}", .{warning});
            } else {
                try memProbe(alloc, freeram, options);
            }
            break;
        }
    }
}

test "suite" {
    _ = @import("bytes.zig");
    _ = @import("options.zig");
    _ = @import("out.zig");
}
